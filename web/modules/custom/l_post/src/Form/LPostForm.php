<?php

namespace Drupal\l_post\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the post entity edit forms.
 */
class LPostForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New post %label has been created.', $message_arguments));
      $this->logger('l_post')->notice('Created new post %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The post %label has been updated.', $message_arguments));
      $this->logger('l_post')->notice('Updated new post %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.l_post.canonical', ['l_post' => $entity->id()]);
  }

}
