<?php

namespace Drupal\l_lemberg\Controller;

use Drupal\l_lemberg\Service\NodeListBuilder;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for building the list of nodes.
 */
class NodeListController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * @var \Drupal\l_lemberg\Service\NodeListBuilder
   */
  protected $nodeListBuilder;

  /**
   * NodeListController constructor.
   *
   * @param \Drupal\l_lemberg\Service\NodeListBuilder $node_list_builder
   */
  public function __construct(
    NodeListBuilder $node_list_builder
  ) {
    $this->nodeListBuilder = $node_list_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection  PhpParamsInspection */
    return new static(
      $container->get('l_lemberg.node_list_builder')
    );
  }

  /**
   * Page callback.
   *
   * @return array
   *   A renderable array with nodes list.
   */
  public function page() {
    return $this->nodeListBuilder->render();
  }

}
