<?php

namespace Drupal\l_lemberg\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for EnvironmentSettings.
 */
class EnvironmentSettingsForm extends FormBase {

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new DevelGenerateForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key value store.
   */
  public function __construct(
    StateInterface $state
  ) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection  PhpParamsInspection */
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'l_environment_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['welcome_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Welcome text'),
      '#default_value' => $this->state->get('l_lemberg.welcome_text'),
      '#required' => TRUE,
    ];

    $form['promotion_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Promotion file'),
      '#default_value' => $this->state->get('l_lemberg.promotion_file'),
      '#upload_validators' => [
        'file_validate_extensions' => [
          'ico png gif jpg jpeg pdf svg',
        ],
      ],
      '#multiple' => FALSE,
      '#upload_location' => 'public://l_lemberg',
      '#progress_indicator' => 'bar',
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save environment settings'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $welcome_text = $form_state->getValue('welcome_text');
    $fids = $form_state->getValue('promotion_file');
    $files = File::loadMultiple($fids);
    foreach ($files as $file) {
      $file->setPermanent();
      $file->save();
    }

    $this->state->set('l_lemberg.welcome_text', $welcome_text);
    $this->state->set('l_lemberg.promotion_file', $fids);
  }

}
