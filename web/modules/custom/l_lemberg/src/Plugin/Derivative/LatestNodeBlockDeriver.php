<?php

namespace Drupal\l_lemberg\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for latest node of type.
 *
 * @see \Drupal\l_lemberg\Plugin\Block\LatestNodeBlock
 */
class LatestNodeBlockDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The node type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * LatestNodeBlockDeriver constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->nodeTypeStorage = $entity_type_manager->getStorage('node_type');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    /* @noinspection  PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $node_types = $this->nodeTypeStorage->loadMultiple();
    foreach ($node_types as $node_type) {
      $plugin_definition = [
        'admin_label' => $this->t('Latest node block:') . ' ' . $node_type->label(),
      ];
      $this->derivatives[$node_type->id()] = $plugin_definition + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
