<?php

namespace Drupal\l_lemberg\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service for build the lists of nodes.
 */
class NodeListBuilder {

  /**
   * The number of entities to list per page, or FALSE to list all entities.
   *
   * For example, set this to FALSE if the list uses client-side filters that
   * require all entities to be listed (like the views overview).
   *
   * @var int|false
   */
  protected $limit;

  /**
   * The entity storage class.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The view mode.
   *
   * @var string
   */
  protected $viewMode;

  /**
   * The entity type ID.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The node view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $viewBuilder;

  /**
   * NodeListBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeId = 'node';
    $this->viewMode = 'teaser';
    $this->limit = 25;
    $this->entityTypeManager = $entity_type_manager;
    $this->storage = $entity_type_manager->getStorage($this->entityTypeId);
    $this->viewBuilder = $entity_type_manager->getViewBuilder($this->entityTypeId);
  }

  /**
   * Builds the entity listing as renderable array.
   */
  public function render() {
    $build = [];
    /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
    foreach ($this->load() as $entity) {
      if ($entity_build = $this->buildEntity($entity)) {
        $group = $entity->get('type')->entity->label();
        if (empty($build['content'][$group])) {
          $build['content'][$group] = [
            '#type' => 'details',
            '#title' => $group,
            '#attributes' => ['class' => ['entity-group-wrapper']],
          ];
        }
        $build['content'][$group][$entity->id()] = $entity_build;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $build;
  }

  /**
   * Builds an entity in the entity listing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   A render array structure of fields for this entity.
   */
  public function buildEntity(EntityInterface $entity) {
    return $this->viewBuilder->view($entity, $this->viewMode);
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_ids = $this->getEntityIds();

    return $this->storage->loadMultiple($entity_ids);
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->storage->getQuery()
      ->sort('created', 'DESC')
      ->sort('type', 'DESC');

    // Only add the pager if a limit is specified.
    if (!empty($this->limit)) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }

}
