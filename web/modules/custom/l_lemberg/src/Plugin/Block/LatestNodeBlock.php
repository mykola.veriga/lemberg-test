<?php

namespace Drupal\l_lemberg\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'LatestNodeBlock' block plugin.
 *
 * @Block(
 *   id = "l_latest_node_block",
 *   admin_label = @Translation("Node block"),
 *   deriver = "\Drupal\l_lemberg\Plugin\Derivative\LatestNodeBlockDeriver"
 * )
 */
class LatestNodeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The node view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $viewBuilder;

  /**
   * The last node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Creates a NodeBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->viewBuilder = $entity_type_manager->getViewBuilder('node');
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->node = $this->loadLatestNode($this->getDerivativeId());
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /* @noinspection  PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * Load the latest Node by node from Deriver.
   *
   * @param string $node_type
   *   The node_type.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The latest node.
   */
  protected function loadLatestNode(string $node_type) {
    $nids = $this->nodeStorage->getQuery()
      ->condition('type', $node_type)
      ->sort('created', 'DESC')
      ->range(0, 1)
      ->execute();

    if (empty($nids)) {
      return NULL;
    }

    return $this->nodeStorage->load(reset($nids));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    if ($this->node instanceof NodeInterface) {
      $build = $this->viewBuilder->view($this->node, $this->configuration['view_mode']);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(
    AccountInterface $account,
    $return_as_object = FALSE
  ) {
    if ($this->node instanceof NodeInterface) {
      return $this->node->access('view', $account, TRUE);
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#options' => $this->viewModesByBundle($this->getDerivativeId()),
      '#defalt_value' => $this->configuration['view_mode'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      $this->configuration[$key] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_mode' => "full",
    ];
  }

  /**
   * Get view modes by node type.
   *
   * @param string $node_type
   *   The node_type.
   *
   * @return array
   *   The view mode options.
   */
  protected function viewModesByBundle(string $node_type) {
    // Always show full as an option, even if the display is not enabled.
    $view_mode_options = [
      'full' => $this->t('Full'),
    ];

    $view_mode_by_bundle = $this
      ->entityDisplayRepository
      ->getViewModeOptionsByBundle('node', $node_type);
    // Unset view modes that are not used in the front end.
    unset($view_mode_by_bundle['default']);
    unset($view_mode_by_bundle['rss']);
    unset($view_mode_by_bundle['search_index']);

    return $view_mode_options + $view_mode_by_bundle;
  }

}
